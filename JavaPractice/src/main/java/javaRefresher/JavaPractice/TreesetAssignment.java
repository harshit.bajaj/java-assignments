package javaRefresher.JavaPractice;

import java.util.Iterator;
import java.util.TreeSet;

public class TreesetAssignment {
	public static void main(String[] args) {
        TreeSet<Integer> n = new TreeSet<>();
        n.add(2);
        n.add(5);
        n.add(6);
        System.out.println("TreeSet: " + n);

        Iterator<Integer> iterate = n.iterator();
        while(iterate.hasNext()) {
            System.out.print(iterate.next());
            System.out.print(", ");
        }
    }
}
