package javaRefresher.JavaPractice;

public abstract class BookAssignment {

	public abstract void read();
	public abstract void write();
}
