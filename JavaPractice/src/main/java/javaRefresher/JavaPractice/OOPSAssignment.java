package javaRefresher.JavaPractice;

public class OOPSAssignment extends InheritanceParent implements BasicCarAssignment{

	@Override
	public void gearChange() {
		System.out.println("Interface method 1");	
	}

	@Override
	public void music() {
		System.out.println("Interface method 2");	
	}
	
	public void oopsSample() {
		System.out.println("Method Overloading Method with 0 args");
	}
	
	public void oopsSample(int a, String b) {
		System.out.println("Method Overloading Method with 2 args");
	}
	
	public void oopsSample(String a, int b) {
		System.out.println("Method Overloading Method with different order");
	}
	
	public void food1() {
		System.out.println("Method overriding");
	}
	
	public static void main(String[] args) {
		InheritanceParent p1=new InheritanceParent();
		OOPSAssignment o1= new OOPSAssignment();
		//----------Inheritance----------
		p1.food1();
		//---------Polymorphism---------
		o1.oopsSample();
		o1.oopsSample(5, "ABC");
		o1.oopsSample("ABC", 5);
		o1.food1();
		//---------Abstraction------------
		o1.gearChange();
		o1.music();
		
		//-----------Encapsulation---------
		EncapsulationPractice e1 = new EncapsulationPractice();
		e1.setId(2);
		e1.setName("ABC");
		System.out.println(e1.getId());
		System.out.println(e1.getName());
	}
	
	

	
}
