package javaRefresher.JavaPractice;

import java.util.ArrayList;
import java.util.List;

public class ArrayListPractice {
public static void main(String[] args) {
	
	ArrayList<String> list = new ArrayList<>();
	System.out.println(list.size());
	list.add("a");
	list.add("b");
	list.add("c");
	list.add("d");
	list.add("e");
	System.out.println(list.size());
	list.set(1,"B");
	list.remove(3);
	list.remove(String.valueOf("e"));
	System.out.println(list.toString());
}   
}
