package javaRefresher.JavaPractice;

public interface BasicCarAssignment {

	public abstract void gearChange();
	public abstract void music();
}
