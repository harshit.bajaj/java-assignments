package javaRefresher.JavaPractice;

public abstract class AbstractClassPractice {

	public abstract void sample001();
	public abstract void sample003();
	public void sameple002() {
		
	}
	//an abstract class is a class which consists of abstract and non-abstract methods
	//an abstract method is a method which constants only declaration and not body.
	//abstract class cant be instantiated
	// a compile time error will be generated for each abstract method (that you don't override in child class)
}
