package javaRefresher.JavaPractice;

public class InheritanceChild2 extends InheritanceParent {

	public void food5() {
		System.out.println("Child2 food");
	}
	public static void main(String[] args) {
		InheritanceParent p1 = new InheritanceParent();
		InheritanceChild2 c2 = new InheritanceChild2();
		p1.food1();
		c2.food1();
		//p1.food5();
		c2.food5();
		//c2.food4();
	}
}
