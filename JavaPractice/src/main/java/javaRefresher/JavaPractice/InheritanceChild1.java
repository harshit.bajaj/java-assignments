package javaRefresher.JavaPractice;

public class InheritanceChild1 extends InheritanceParent{
	
	public void food4() {
		System.out.println("Child1 food");
	}
	public static void main(String[] args) {
		InheritanceParent p1 = new InheritanceParent();
		InheritanceChild1 c1 = new InheritanceChild1();
		p1.food1();
		c1.food1();
		//p1.food4();
		c1.food4();
		//c1.food5();
	}
}
