package javaRefresher.JavaPractice;

import java.util.HashMap;

public class HashMapAssignment {
public static void main(String[] args) {
	HashMap<Integer,Boolean> map = new HashMap<>();
	map.put(1,true);
	map.put(2, false);
	map.put(3,false);
	map.put(4, true);
	map.put(5,true);
	map.put(6, true);
	map.put(7,true);
	map.put(18, false);
	
	for(int i: map.keySet()) {
		if(map.get(i)==false)
			System.out.println(i);
	}
}
}
