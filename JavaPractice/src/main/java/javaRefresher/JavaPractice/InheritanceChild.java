package javaRefresher.JavaPractice;

public class InheritanceChild extends InheritanceParent{
	
	public void food2() {
		System.out.println("Child food");
		food1();
	}
	
	public static void main(String[] args) {
		InheritanceParent p1 = new InheritanceParent();
		InheritanceChild c1 = new InheritanceChild();
		InheritanceGrandParent g1 = new InheritanceGrandParent();
		p1.food1();
		c1.food2();
		c1.food1();
		//p1.food2();
		c1.food3();
		p1.food3();
		g1.food3();
//		g1.food1();
//		g1.food2();
	}
}
