package javaRefresher.JavaPractice;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LinkedListPractice {
	public static void main(String[] args) {
		
		LinkedList<String> list = new LinkedList<>();
		System.out.println(list.size());
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		list.add(1, "a1");
		list.addFirst("Z");
		list.addLast("w");
		list.remove("d");
		System.out.println(list);
		System.out.println(list.size());
		list.set(1,"B");
		list.remove(3);
		list.remove(String.valueOf("e"));
		System.out.println(list.size());
		System.out.println(list.toString());
	}   
}
//a points to b
//b points to c
//c points to d
//d points to e
