package javaRefresher.JavaPractice;

public class PatternPracticeAndAssignment {
		int a = 0;//instance var
		static int b = 2; //static var
		public void sample() {
			int c = 3; //local var
			a+=5;
			System.out.println(a);
		}
		public static void main(String[] args) {
			PatternPracticeAndAssignment p1= new PatternPracticeAndAssignment();
			PatternPracticeAndAssignment p2= new PatternPracticeAndAssignment();
			p1.patternDNA();
			p1.patternRightAngleTriangle();
//			a=a+20;//we cant call a instance var without creating an obj
//			System.out.println(a);
//			b-=5;// we can access static var with using an obj
//			System.out.println(b);
//			c*=5;// local var cant be accessed outside its scope/block
//			System.out.println(c);
		}
		
		public void patternDNA() {
			System.out.println("---------DNA----------");
			for(int i=1;i<5;i++) {
				System.out.print(" "+i);
				System.out.println();
				for(int j=1;j<3;j++) {
					System.out.print(j+" ");
				}
				System.out.println();
			}
			
		}
		
		public void patternRightAngleTriangle() {
			System.out.println("---------Right angle triangle----------");
			int count=0;
			for(int i=0;i<1;i++) {
				System.out.print(" "+i);
				System.out.println();
				for(int j=1;j<8;j++) {
					if(count==3)
						System.out.println();
					System.out.print(" "+j+" ");
					count++;
				}
			}
		}
}
