package javaRefresher.JavaPractice;

public class ArrayPracticeAndAssignments {	
	public static void main(String[] args) {
		//----1-D------
		System.out.println("1-D");
		int arr[] = new int[]{1,2,3,4,5};
		for(int i=0;i<arr.length;i++) {
			System.out.println(arr[i]);
		}
		
		System.out.println();
		//-----2-D-----------
		System.out.println("2-D");
		int arr1[][] = {{5,10},{2,3},{3,9,6}};
		for(int i=0;i<arr1.length;i++) {
			for(int j=0;j<arr1[i].length;j++) {
				System.out.print(arr1[i][j]+" ");
			}
			System.out.println();
		}
		
		System.out.println();
		//------assignemnt quetion-----------
		System.out.println("Assignemnt question 1");
		int arr2[] = new int[]{1,2,3,4,5};
		int sum=0;
		for(int i=0;i<arr2.length;i++) {
			sum+=arr2[i];
		}
		System.out.println("Avgerage = "+sum/arr2.length);
		
		System.out.println();
		System.out.println("Assignemnt question 2");
		int arr3[][] = new int[][]{{1},{8,2},{3,4},{5,0}};
		for(int i=0;i<arr3.length;i++) {
			for(int j=0;j<arr3[i].length;j++) {
			if(arr3[i][j]%2==0) {
				System.out.println(arr3[i][j]+ " is even ");
			}
			else
				System.out.println(arr3[i][j]+" is odd ");
			}
		}
	}
	
	
}
