package javaRefresher.JavaPractice;

import java.util.HashMap;

public class StringPracticeAndAssignment {
	public static void main(String[] args) {
		String s1 = "Welcome";//String constant pool
		String s2 = "Welcome";//String constant pool
		String s3 = new String("Welcome");//Heap memory
		String s4 = new String("Hello World");//Heap memory
		char[] ch = {'S','e','l','e','n','i','u','m'};
		String s5 = String.valueOf(ch);
		System.out.println(s1+" "+s2+" "+s3+" "+s4+" "+s5);

		//we are changing the reference here
		s1.concat("Java");
		System.out.println(s1);
		s1=s1.concat("Java");
		System.out.println(s1);

		String s6 = "Welcome";
		String s7 = "Java";

		//equals method just compare the content not where it is stored or reference
		System.out.println(s6.equals(s2)+" "+ s6.equals(s3)+" "+ s6.equals(s7)+" "+ s2.equals(s3));

		// '==' compare content with where it is stored or reference
		System.out.println((s6==s2)+" "+ (s6==s3)+" "+ (s6==s7)+" "+ (s2==s3));
		System.out.println();
		twoQuestions();
		System.out.println();
		palindrome();
		System.out.println();
		frequencyOfElement();
		System.out.println();
		globallogicIsPresent();
	}

	public static void globallogicIsPresent() {
		System.out.println("----Write a program to check if the word Globallogic is present in the sentence -Welcome to Globallogic----");
		String n = "Welcome to Globallogic";
		int x=0;
		String c1[] = n.split("\\s+");
		for(String c: c1) {
			if(c.equals("Globallogic")) {
				x=1;
				break;
			}      
		}
		if(x==0)
			System.out.println("No");
		else
			System.out.println("Yes");
}

	public static void twoQuestions() {
		System.out.println("-----------------Two Ques Each----------------------------------------------");
		System.out.println("Q1. String- Length of each word in a string");
		String n1 = "Java Training Sessions";
		String c1[] = n1.split("\\s+");
		for(String c: c1) {
			System.out.println("Length of "+ c+ " is: "+ c.length());
		}
		System.out.println("Q2. String- Print only digits in a string");
		String n2 = "Jav12a5";
		char ch[] = n2.toCharArray();
		for(char c: ch) {
			if(Character.isDigit(c))
				System.out.print(c+" ");
		}
		System.out.println();
		System.out.println("----------------------------------");

		System.out.println("Q1. StringBuilder- Reverse a string");
		String n3 = "Java";
		StringBuilder sb = new StringBuilder(n3);
		sb.reverse();
		System.out.println(sb.toString());
		System.out.println("Q2. StringBuilder- Remove spaces from string");
		StringBuilder sb1 = new StringBuilder("Java Training");
		for(int i=0;i<sb1.length();i++) {
			if(sb1.charAt(i)==' ') {
				sb1.replace(i, i+1, "");
			}
		}
		System.out.println(sb1.toString());

		System.out.println("----------------------------------");

		System.out.println("Q1. StringBuffer- Reverse a string");
		String n4 = "Java";
		StringBuffer sb2 = new StringBuffer(n3);
		sb2.reverse();
		System.out.println(sb2.toString());
		System.out.println("Q2. StringBuffer- Remove spaces from string");
		StringBuffer sb3 = new StringBuffer("Java Training");
		for(int i=0;i<sb3.length();i++) {
			if(sb3.charAt(i)==' ') {
				sb3.replace(i, i+1, "");
			}
		}
		System.out.println(sb1.toString());

	}

	public static void frequencyOfElement() {
		System.out.println("-----------Frequency of each element-------------------------------------");
		String n = "aba";
		HashMap< Character, Integer> map = new HashMap<>();
		char ch[] = n.toCharArray();
		for(char c: ch) {
			if(map.containsKey(c))
				map.put(c, map.get(c)+1);
			else
				map.put(c, 1);
		}
		System.out.println("Frequency of each element in word "+ n+ " : "+ map.toString());
	}

	public static void palindrome() {
		System.out.println("-----------Palindrome---------------------------------------------------");
		String n = "aba";
		StringBuilder sb = new StringBuilder(n);
		sb.reverse();
		String n1 = sb.toString();
		if(n.equals(n1))
			System.out.println(n+" is palindrome");
		else
			System.out.println(n+" is not palindrome");
	}
}
